import React from 'react';
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import { useDispatch } from 'react-redux';
import { clearCart } from '../Cart/CartSlice';
import { closeModal } from './Model/Modelslice';

export default function Model() {
    const dispatch = useDispatch()
    return (
        <>
            <Modal show={true} >
                <Modal.Header >
                    <Modal.Title>Remove All ItemsFrom Your Shopping Cart ?</Modal.Title>
                </Modal.Header>

                <Modal.Footer className='d-flex justify-content-between'>
                    <Button variant="primary" onClick={() => {
                        dispatch(clearCart())
                        dispatch(closeModal())
                    }}>
                        CONFIRM
                    </Button>
                    <Button variant="danger" onClick={() => dispatch(closeModal())}>
                        CANCLE
                    </Button>
                </Modal.Footer>
            </Modal>
        </>
    );
}
