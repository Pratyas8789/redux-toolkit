import React from 'react'
import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import { useSelector } from 'react-redux';

export default function NavBar() {
  const { amount } = useSelector((store) => store.cart)
  return (
    <Navbar bg="primary" variant="dark">
      <Container>
        <Navbar.Brand href="#home">Redux Toolkit</Navbar.Brand>
        <Nav className="nav-center">
          <i className="fa fa-cart-arrow-down fa-2x" style={{ color: "white" }} aria-hidden="true">{amount}</i>
        </Nav>
      </Container>
    </Navbar>
  )
}
