import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import Card from 'react-bootstrap/Card';
import CartItems from './CartItems';
import Button from 'react-bootstrap/Button';
import { openModal } from './Model/Modelslice';


export default function CartContainer() {
  const dispatch = useDispatch()
  const { cartItems, total, amount } = useSelector((store) => store.cart)

  if (amount < 1) {
    return (
      <section className='d-flex justify-content-center' >
        <Card style={{ width: '18rem', height: "10rem" }}>
          <Card.Body className='d-flex flex-column justify-content-center align-items-center' >
            <Card.Title className='m-3'>YOUR BAG</Card.Title>
            <Card.Subtitle className="mb-2 text-muted">is currently empty</Card.Subtitle>
          </Card.Body>
        </Card>
      </section>
    )
  }
  return (
    <section className='d-flex flex-column align-items-center' >
      <header className='d-flex justify-content-center align-items-center'>
        <h2>Your bag</h2>
      </header >
      <div className='d-flex flex-column ' style={{ width: "90%" }}>
        {
          cartItems.map((item) => {
            return <CartItems key={item.id} {...item} />
          })
        }
      </div>
      <footer style={{ width: "90%" }} >
        <hr />
        <div className='d-flex flex-column  align-items-center'  >
          <h4 style={{ width: "100%" }} className='d-flex justify-content-between align-items-center' >
            total
            <span> ${total.toFixed(2)}</span>
          </h4>
          <br />
          <br />
          <Button variant="danger" onClick={() => dispatch(openModal())}>CLEAR CART</Button>{' '}
        </div>
      </footer>
    </section>
  )
}
