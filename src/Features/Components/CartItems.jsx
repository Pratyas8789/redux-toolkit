import React from 'react'
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import { decrease, increase, removeItem } from '../Cart/CartSlice';
import { useDispatch } from 'react-redux';

export default function CartItems({ id, img, price, amount, title }) {
    const dispatch = useDispatch();

    return (
        <Card className='d-flex flex-row mt-3' style={{ width: '100%', height: "200px" }}>

            <Card.Img style={{ height: "100%", width: "15%" }} src={img} />
            <Card.Body style={{ height: "100%", width: "70%" }}>
                <Card.Title>{title}</Card.Title>
                <Card.Text>$ {price}</Card.Text>
                <Button variant="danger" onClick={() => dispatch(removeItem(id))} >remove</Button>
            </Card.Body>

            <Card.Body className='d-flex flex-column align-items-center justify-content-between' style={{ height: "100%", width: "15%" }}>

                <Button variant="light" onClick={() => dispatch(increase(id))} ><i className="fa fa-arrow-up" aria-hidden="true"></i></Button>{' '}
                <Card.Text>{amount}</Card.Text>
                <Button variant="light" onClick={() => {
                    if (amount === 1) {
                        dispatch(removeItem(id))
                        return
                    }
                    dispatch(decrease(id))
                }} >
                    <i className="fa fa-arrow-down" aria-hidden="true"></i>
                </Button>{' '}

            </Card.Body>
        </Card>

    )
}
