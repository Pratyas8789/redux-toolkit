import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
// import cartItems from "../../cart";

const url = "https://course-api.com/react-useReducer-cart-project"

const initialState = {
    cartItems: [],
    amount: 0,
    total: 0,
    isloading: true
}

export const getCartItems = createAsyncThunk('cart/getCartItems', () => {
    return fetch(url)
        .then((res) => res.json())
        .catch((err) => console.log(err))
})

const cartSlice = createSlice({
    name: 'cart',
    initialState,
    reducers: {
        clearCart: (state) => {
            state.cartItems = []
            // return {cartItems:[]}
        },
        removeItem: (state, action) => {
            const itemId = action.payload
            state.cartItems = state.cartItems.filter((item) => item.id !== itemId)
        },
        increase: (state, { payload }) => {
            console.log(payload);
            const item = state.cartItems.find((item) => item.id === payload)
            item.amount = item.amount + 1
        },
        decrease: (state, { payload }) => {
            const item = state.cartItems.find((item) => item.id === payload)
            item.amount = item.amount - 1
        },
        calculateTotal: (state) => {
            let amount = 0;
            let total = 0;
            state.cartItems.map((item) => {
                amount += item.amount
                total += item.amount * item.price
            });
            state.amount = amount
            state.total = total
        }
    },
    extraReducers: {
        [getCartItems.pending]: (state) => {
            state.isloading = true
        },
        [getCartItems.fulfilled]: (state,action) => {
            console.log(action);
            // console.log(state);
            state.isloading = false
            state.cartItems=action.payload
        },
        [getCartItems.rejected ]: (state) => {
            state.isloading = false
        },
    }
})
export const { clearCart, removeItem, increase, decrease, calculateTotal } = cartSlice.actions
export default cartSlice.reducer