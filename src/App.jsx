import { useEffect } from 'react'
import './App.css'
import NavBar from './Features/Components/NavBar'
import CartContainer from './Features/Components/CartContainer'
import { useDispatch, useSelector } from 'react-redux'
import { calculateTotal, getCartItems } from './Features/Cart/CartSlice'
import Model from './Features/Components/Model'

function App() {
  const { cartItems, isloading } = useSelector((store) => store.cart)
  const { isOpen } = useSelector((store) => store.modal)
  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(calculateTotal())
  }, [cartItems])

  useEffect(() => {
    dispatch(getCartItems());
  }, [])

  if (isloading) {
    return <div>
      <h1>Loading</h1>
    </div>
  }
  return (
    <div className="App">
      {isOpen && < Model />}
      <NavBar />
      <CartContainer />
    </div>
  )
}
export default App